import {Injectable} from '@angular/core';
import {UserService} from 'src/app/services/user.service';
import {AngularFirestore, AngularFirestoreCollection} from '@angular/fire/firestore';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {Order} from '../models/order.model';

@Injectable({
    providedIn: 'root'
})


export class OrderUserService {
    orders: Observable<any>;
    ordersDB: AngularFirestoreCollection<Order>;

    constructor(public DB: AngularFirestore, private userService: UserService) {
        var profileRef = this.userService.getProfileData();
        var userID = profileRef.uid;

        this.ordersDB = this.DB.collection('orders');
        this.orders = this.DB.collection('orders').snapshotChanges().pipe(map(changes => {
            return changes.map(a => {
                const data = a.payload.doc.data() as Order;
                data.id = a.payload.doc.id;

                if (data.uid == userID) {
                    return data;
                }
                // else returns undefined
            });
        }));
    }

    // tslint:disable-next-line:use-life-cycle-interface
    ngOnInit() {
    }


    getOrdersByID() {
        return this.orders;
    }
}
