import { Injectable } from '@angular/core';
import { Category } from 'src/app/models/category.model';
import { AngularFirestore, AngularFirestoreDocument, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import {map} from 'rxjs/operators';


@Injectable({
    providedIn: 'root'
  })

export class CategoryService {
    productDBCategory: AngularFirestoreCollection<Category>;
    productCategory: Observable<any>;
    productDocCategory: AngularFirestoreDocument<Category>;
    uid: string;

    constructor(public DB: AngularFirestore) {
        this.productDBCategory = this.DB.collection('categories');
        this.productCategory = this.DB.collection('categories').snapshotChanges().pipe(map(changes => {
            return changes.map(a => {
              const data = a.payload.doc.data() as Category;
              data.id = a.payload.doc.id;
              return data;
          });
         }));

     }

    createCategory(category: Category) {
        this.generateId(category);
        this.DB.collection('categories').doc(category.name).set({
            name: category.name,
            id: category.id
        });

    }

    updateCategory(category: Category, newname: String) {
        this.DB.collection('categories').doc(category.id).update({
            name : newname,
        });
    }

    deleteCategory(category: Category) {
        this.DB.collection('categories').doc(category.id).delete();

    }

    getCategories() {
        return this.productCategory;
    }

   generateId(category: Category) {
        category.id =  Math.random().toString(36).substring(2, 12) + Math.random().toString(36).substring(8, 19);
    }

}
