import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireStorage } from '@angular/fire/storage';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';

import { Product } from "src/app/models/product.model";

@Injectable({
  providedIn: 'root'
})
export class ProductAdminService {

  constructor (
    private afs: AngularFirestore,
    private storage: AngularFireStorage
  ) {

  }

  ngOnInit() {  }

  createProduct(product: Product): Promise<firebase.firestore.DocumentReference> {
    product.imageURL = "";
    product.rating = 0;
    product.ratings = {};

    return this.afs.collection(`products`).add(product);
  }

  updateProduct(productId: string, productData): Promise<void> {
    delete productData.id;

    return this.afs.doc(`products/${productId}`).update(productData);
  }

  deleteProduct(productId: string){
    this.afs.collection('products').doc(productId).delete();
    this.storage.ref(`products-images/${productId}`).delete();
  }

  uploadImage(imageFile: Blob, productId: string) {
    // Upload an image in the storage
    /* Return a promise :
        .then(imageURL: string)
        .catch(error: Object)
    */
    return new Promise((resolve, reject) => {
      this.storage.upload(`products-images/${productId}`, imageFile)
        .then(imageRef => {
          // Image upload succeed

          imageRef.ref.getDownloadURL().then(imageURL => {
            resolve(imageURL);
          })
        })
        .catch(reject) // Upload Image error
    })
  }

  createProductWithImage(product: Product, imageFile: Blob): Promise<Object> {
    // Create a product in the database
    // Upload the image imageFile and update the product with the image URL
    /* Return a promise :
        .then(void)
        .catch({
          error: "product" or "image"
        })
    */
    return new Promise((resolve, reject) => {
      this.createProduct(product)
        .then(productRef => {
          // Product creation succeed

          productRef.get().then(productSnapshot => {
            this.uploadImage(imageFile, productSnapshot.id)
              .then(imageURL => {

                this.updateProduct(productSnapshot.id, { imageURL: imageURL })
                  .then(_ => resolve()); // Image correctly associated to the product, total success
              })
              .catch(error => reject({ error: "image" })) // Image upload error, but the product is still created
          })
        })
        .catch(_ => reject({ error: "product" })) // Create Product error
    })
  }

}
