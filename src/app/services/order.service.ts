import {Injectable} from '@angular/core';
import {AngularFirestore, AngularFirestoreCollection} from '@angular/fire/firestore';
import {Order} from '../models/order.model';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';


@Injectable({
    providedIn: 'root'
})
export class OrderService {
    private size: number;
    orders: Observable<any>;
    ordersDB: AngularFirestoreCollection<Order>;


    constructor(public DB: AngularFirestore) {
        this.ordersDB = this.DB.collection('orders');
        this.orders = this.DB.collection('orders').snapshotChanges().pipe(map(changes => {
            return changes.map(a => {
                const data = a.payload.doc.data() as Order;
                data.id = a.payload.doc.id;
                return data;
            });
        }));
    }

    getOrders() {
        return this.DB.collection('/orders/').valueChanges();
    }
    getOrderDetails(id) {
        return this.DB.collection('/orders/').doc(id).get();
    }

    updateOrder(id, status) {
        return this.DB.collection('orders').doc(id).set({
            status
        }, {
            merge: true
        });
    }

    createOrder(order: Order) {
        this.DB.collection('orders').doc(order.id).set({
            name: order.name,
            id: order.id,
            datePlaced: order.datePlaced,
            uid: order.uid,
            description: order.description,
            status: order.status,
            address: order.address,
            city: order.city,
            email: order.email
        });

    }

}
