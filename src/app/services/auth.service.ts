import { Injectable } from '@angular/core';
import { AngularFireAuth } from "@angular/fire/auth";
import { Router } from "@angular/router";
import { UserService } from 'src/app/services/user.service';


@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private userData: any; // Save logged in user data

  constructor(
    private afAuth: AngularFireAuth, // Inject Firebase auth service
    private router: Router,
    private userService: UserService,
  ) {
    let userDataLocalStorage = localStorage.getItem("user");

    if (userDataLocalStorage === null) {
      this.userData = null
    }
    else {
      
      this.userData = JSON.parse(userDataLocalStorage);
      this.userService.setProfileData(this.userData.uid, this.userData.email);
    }
  }
  
  signUp (email: string, password: string) {
    return new Promise((resolve, reject) => {
      this.afAuth.auth.createUserWithEmailAndPassword(email, password)
        .then(user => {
          this.afAuth.auth.currentUser.sendEmailVerification()
          this.userService.createUser(user);
          resolve(user)
        })
        .catch(err => reject(err))
    })
  }

  isLoggedIn(): boolean {
    return this.userData !== null;
  }

  getUserID(): string {
      if (this.isLoggedIn()) {
          return this.userData.uid;
      }
      else {
          return null;
      }
  }

  // The user is not logged in if their email address is not verified
  private onSuccessfulLogin (userData, resolve, reject) {
    if (userData.user.emailVerified === false) {
      reject({code: "unverified-email"})
      return;
    }
    
    this.userData = userData.user; // Be careful with the context of "this", the function can't be used as a callback
                                  // if the scope is not the scope of the class
    this.userService.setProfileData(this.userData.uid, this.userData.email);
    localStorage.setItem("user", JSON.stringify(this.userData));
    resolve();
  }

  logIn(email: string, password: string) {
    return new Promise((resolve, reject) => {
       this.afAuth.auth.signInWithEmailAndPassword(email, password)
        .then(user => this.onSuccessfulLogin(user, resolve, reject))
        .catch(error => {
          reject(error);
        });
    })
  }

  // Auth logic to run auth providers
  /*authLogin (provider) {
    return new Promise((resolve, reject) => {
      this.afAuth.auth.signInWithPopup(provider)
        .then(user => this.onSuccessfulLogin(user, resolve, reject))
        .catch((error) => {
          window.alert(error)
        })
    })
  }*/

  signOut() {
    return this.afAuth.auth.signOut().then(() => {
      this.userData = null;
      localStorage.removeItem('user');
      this.userService.resetUser();
      this.router.navigate(['/']);
    })
  }

  resetPassword(email: string) {
    return new Promise((resolve, reject) => {
      this.afAuth.auth.sendPasswordResetEmail(email)
        .then(() => resolve())
        .catch(err => reject(err))
    })
  }

}
