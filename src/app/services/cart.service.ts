import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Product } from '../models/product.model';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from '@angular/fire/firestore';
import { map } from 'rxjs/operators';
import { AuthService } from './auth.service';
import { UserService } from './user.service';
import { visitAll } from '@angular/compiler';
import { reject } from 'q';
import { nextContext } from '@angular/core/src/render3';
import { promise } from 'selenium-webdriver';

@Injectable({
    providedIn: 'root'
})
export class CartService {

    private cartCollectionRef: AngularFirestoreCollection<cart>;
    private cartDocumentRef: AngularFirestoreDocument<cart>;
    private UID: string;
    private cartID: string;
    private cart: cart;


    constructor(
        private afs: AngularFirestore,
        private auth: AuthService
    ) {
        this.init()
    }

    //Call this method first when implementing cart service
    //this starts the service by grabbing the cart for the currently logged in user
    //use the promise to avoid retrieving data from service before DB query is complete
    init(): Promise<cart> {
        return new Promise((resolve, reject) => {
            //handle the case that a user is not logged in
            if (!this.auth.isLoggedIn()) {
                alert("You should be logged in..")
                reject("User not logged in");
            } else {
                this.UID = this.auth.getUserID();
                //Get the cart if it exists, if not the constructor is call createCart and
                //use this.cart to represent cart data
                this.cartCollectionRef = this.afs.collection<cart>('carts');
                this.cartCollectionRef.doc<cart>(this.UID).ref.get().then(
                    (result) => {
                        if (result.exists) {
                            this.cartID = result.id;
                            this.cart = result.data() as cart;
                            resolve(this.cart);
                        } else {
                            this.cartID = null;
                            this.createCart();
                            resolve(this.cart);
                        }
                    }
                )
            }
        })
    }

    //a function to save cart changes in this.cart to the DB
    //this should be used after every cart manipulation function
    private updateCart() {
        this.cartCollectionRef.doc<cart>(this.UID).set(this.cart).catch(err => {
            console.log("Cart Service Error: " + err);
        });
    }

    // A simpler method for me so I can use the prodID to check the quantity of the products.
    // So that the user cant specify a higher quantity than the quantity available
    getCartItemByPID(thisPID: string): cartItem {
        this.cart.Products.forEach(value => {
            if (value.PID == thisPID) {
                return value as cartItem;
            }
        })

        return null;
    }

    getCartItemIndex(thisPID: string): number {
        let pos: number;
        this.cart.Products.forEach((value, index) => {
            if (value.PID == thisPID) {
                pos = index;
            }
        })

        if (pos == undefined) {
            return null;
        } else {
            return pos;
        }
    }

    //add item to users cart
    //returns true for success
    //if item is already there then add one to quantity
    addItemToCart(thisPID: string): boolean {
        let productIndex: number = this.getCartItemIndex(thisPID);

        //if product found increment the quantity
        if (productIndex != null) {
            this.cart.Products[productIndex].Quantity++;
            this.updateCart();
            return true;
        } else {
            let newProduct: cartItem = {
                PID: thisPID,
                Quantity: 1
            }
            this.cart.Products.push(newProduct);
            this.updateCart();
            return true;
        }

        return false;
    }

    //Removes an item/items from the cart regardless of quantity
    removeItemFromCart(thisPID: string) {
        let productIndex: number = this.getCartItemIndex(thisPID);

        //item exists, delete it
        if (productIndex != null) {
            this.cart.Products.forEach((value, index) => {
                if (value.PID == thisPID) {
                    this.cart.Products.splice(index, 1);
                    this.updateCart();
                    return true;
                }
            })
        } else { //item does not exist
            return true;
        }

        return false;
    }

    //if quantity is set to zero then remove item
    setQuantity(thisPID: string, thisQuantity: number): boolean {
        this.cart.Products.forEach((value, index) => {
            if (value.PID == thisPID) {
                this.cart.Products[index].Quantity = thisQuantity;
                this.updateCart();
                return true;
            }
        })
        return false;
    }

    //return an array of items that are in the cart
    // Used to display all the items in the cart
    getItemsFromCart(): cartItem[] {
        return this.cart.Products;
    }

    //used is current user does not have a car doc in the DB
    //do not use this, is called in constructor if we cant find DB for user.
    private createCart(): boolean {
        //user car already exists
        if (this.cartID != undefined) { return false; }
        let cart: cart = {
            Products: []
        }

        this.cartCollectionRef.doc<cart>(this.UID).set(cart).catch(
            err => {
                return false;
            }
        );
        return true;
    }

}


