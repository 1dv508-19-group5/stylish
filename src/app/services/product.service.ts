import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { map, switchMap } from 'rxjs/operators';
import { Observable } from 'rxjs';

import { Product } from "src/app/models/product.model";
import * as firebase from 'firebase';
import { $ } from 'protractor';

@Injectable({
  providedIn: 'root'
})
export class ProductService {
  private productsObversable: Observable<any>

  constructor (
    private afs: AngularFirestore,
  ) {
    this.productsObversable = this.afs.collection<Product>("products").snapshotChanges().pipe(
      map(actions => actions.map(a => {
        const data = a.payload.doc.data() as Product;
        data.id = a.payload.doc.id;
        return data;
      }))
    )

    this.afs.collection<Product>("products").snapshotChanges().subscribe(actions => {
      let _actions = actions.map(a => {
        const data = a.payload.doc.data() as Product;
        data.id = a.payload.doc.id;
        return data;
      })

      this.productsObversable = new Observable<Product[]>(observer => {
        observer.next(_actions);
      });
    });
  }
  
  getProducts(){
    return this.productsObversable;
  }

  getProduct (uid: string): Promise<Product> {
    // Get a product given its id
    /* Return a promise :
        .then(product: Product)
        .catch()
    */
    return new Promise((resolve, reject) => {
      this.productsObversable.forEach(products => products.forEach(product => {
        
        if (product.id == uid) {
          return resolve(product);
        }
      })).catch(reason => {
        return reject() // Product not found (no product with this id) or error accessing productsObversable
      });
    })
  }

  hasBought(pid, userID) {
    var docRef=this.afs.collection("products").doc(pid)
    
    docRef.update({
      bought: firebase.firestore.FieldValue.arrayUnion(userID),
    })
  }

  setRating(value, pid,userID){
    var productID=pid
    var docRef=this.afs.collection("products").doc(productID)
    
    docRef.update({
      rating: value,
      ratings: firebase.firestore.FieldValue.arrayUnion(userID),
      
    })
  }
  getPager(totalItems: number, currentPage: number = 1, pageSize: number = 8) {
    // calculate total pages
    let totalPages = Math.ceil(totalItems / pageSize);

    // ensure current page isn't out of range
    if (currentPage < 1) { 
        currentPage = 1; 
    } else if (currentPage > totalPages) { 
        currentPage = totalPages; 
    }
    
    let startPage: number, endPage: number;
    if (totalPages <= 10) {
        // less than 10 total pages so show all
        startPage = 1;
        endPage = totalPages;
    } else {
        // more than 10 total pages so calculate start and end pages
        if (currentPage <= 6) {
            startPage = 1;
            endPage = 10;
        } else if (currentPage + 4 >= totalPages) {
            startPage = totalPages - 9;
            endPage = totalPages;
        } else {
            startPage = currentPage - 5;
            endPage = currentPage + 4;
        }
    }

    // calculate start and end item indexes
    let startIndex = (currentPage - 1) * pageSize;
    let endIndex = Math.min(startIndex + pageSize - 1, totalItems - 1);

    // create an array of pages to ng-repeat in the pager control
    let pages = Array.from(Array((endPage + 1) - startPage).keys()).map(i => startPage + i);

    // return object with all pager properties required by the view
    return {
        totalItems: totalItems,
        currentPage: currentPage,
        pageSize: pageSize,
        totalPages: totalPages,
        startPage: startPage,
        endPage: endPage,
        startIndex: startIndex,
        endIndex: endIndex,
        pages: pages
    };
  }
}
