import { Injectable } from '@angular/core';
import { User } from "src/app/models/user.model";
import { AngularFirestore, AngularFirestoreDocument } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
 
@Injectable({
  providedIn: 'root'
})
 
 
export class UserService {
  private profileRef;
  items: Observable<any[]>;

  constructor (
    private afs: AngularFirestore
  ) { }

  ngOnInit(){}
 
   createUser (user: any) {  
    const userRef: AngularFirestoreDocument<any> = this.afs.doc(`users/${user.user.uid}`);
    const userData: User = {
      uid: user.user.uid,
      name: "",
      address: "",
      zipcode: "",
      city:"",
      admin:false
    }
    userRef.set(userData, {
      merge: true
    })
  }

  updateEverything(newAddress: string, newCity: string, newZipcode: string){
    this.afs.collection("users").doc(this.profileRef['uid']).update({
      address: newAddress,
      city:newCity,
      zipcode:newZipcode,
    })
    this.profileRef['address']=newAddress;
    this.profileRef['city']=newCity;
    this.profileRef['zipcode']=newZipcode;
  }

  updateName(newName: string){
    this.afs.collection("users").doc(this.profileRef['uid']).update({
        name: newName,
      })
      
      this.profileRef['name']=newName;
  }
 
  setProfileData (uid: string, email?: string) { //returns false if user is not found
    this.afs.collection('users', ref => ref.where('uid', '==', uid)).valueChanges().subscribe((next) => {
      let user = next[0];

      if (email != null) user['email'] = email;
      
      this.profileRef = user;
    });
    
  }

  getProfileData () {
      return this.profileRef;
  }

  resetUser () {
      this.profileRef = null;
  }

  isAdmin (): boolean {
      if (this.profileRef == null) {
         return false;
      }
      return this.profileRef.admin;
  }

}
