import {Ng2CarouselamosModule} from 'ng2-carouselamos';

import {BrowserModule} from '@angular/platform-browser';
import {environment} from '../environments/environment';
import {NgModule} from '@angular/core';
import {AngularFireModule} from '@angular/fire';
import {AngularFireAuthModule} from '@angular/fire/auth';
import {AngularFirestoreModule} from '@angular/fire/firestore';
import {AngularFireStorageModule} from '@angular/fire/storage';
import {RouterModule} from '@angular/router';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import {AppComponent} from './app.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

import {CategoryService} from './services/category.service';
import {UserService} from './services/user.service';
import {AuthService} from './services/auth.service';
import {ProductAdminService} from './services/product-admin.service';
import {ProductService} from './services/product.service';
import {OrderUserService} from './services/user-order.service';
import {OrderService} from './services/order.service';

import {AdminGuard} from './core/admin.guard';
import {AuthGuard} from './core/auth.guard';

import {SignupComponent} from './components/signup/signup.component';
import {LoginComponent} from './components/login/login.component';
import {ForgotPasswordComponent} from './components/forgot-password/forgot-password.component';

import {NavbarComponent} from './components/navbar/navbar.component';
import {HomeComponent} from './components/home/home.component';
import {ProductsComponent} from './components/products/products.component';
import {CheckOutComponent} from './components/check-out/check-out.component';
import {FooterComponent} from './components/footer/footer.component';
import {PageNotFoundComponent} from './components/page-not-found/page-not-found.component';
import {ProfileDisplayComponent} from './components/user/profile-display/profile-display.component';
import {UpdateAddressComponent} from './components/user/update-address/update-address.component';


import {AdminRootComponent} from './components/admin/admin-root/admin-root.component';
import {ProductEditComponent} from './components/admin/product-edit/product-edit.component';
import {ProductCreateComponent} from './components/admin/product-create/product-create.component';
import {CategoryComponent} from './components/admin/category/category.component';


import {ConfirmExitModalComponent, NgbdModalFocus} from './components/confirm-exit-modal/confirm-exit-modal.component';
import {SearchResultComponent} from './components/search-result/search-result.component';

import {RootComponent} from './components/root/root.component';
import {UserRootComponent} from './components/user/user-root/user-root.component';

import {HttpClientModule} from '@angular/common/http';
import {DailydealsCarouselComponent} from './components/dailydeals-carousel/dailydeals-carousel.component';

import {ProductPageComponent} from './components/product-page/product-page.component';
import {ConfirmAddressComponent} from './components/user/confirm-address/confirm-address.component';
import {PaymentComponent} from './components/user/payment/payment.component';
import {ShippingAddressComponent} from './components/user/shipping-address/shipping-address.component';

import {CartComponent} from './components/cart/cart.component';
import {CartService} from './services/cart.service';
import {ViewOrdersUserComponent} from './components/user/view-orders-user/view-orders-user.component';

import {OrdersListComponent} from './components/admin/orders-list/orders-list.component';
import {OrderDetailsComponent} from './components/admin/order-details/order-details.component';

import {ConfirmOrderComponent} from './components/user/confirm-order/confirm-order.component';
import {ThankYouComponent} from './components/user/thank-you/thank-you.component';
import { ShoppingCartComponent } from './components/shopping-cart/shopping-cart.component';
import { UpdatePasswordComponent } from './components/user/update-password/update-password.component';

@NgModule({
    declarations: [
        AppComponent,
        NavbarComponent, FooterComponent,
        HomeComponent,
        ProductsComponent,
        CheckOutComponent,
        SignupComponent, LoginComponent, ForgotPasswordComponent,
        CategoryComponent,
        ProfileDisplayComponent,
        UpdateAddressComponent,
        PageNotFoundComponent,
        ConfirmExitModalComponent,
        NgbdModalFocus,
        SearchResultComponent,
        ProductCreateComponent,
        ProductEditComponent,
        SearchResultComponent,
        AdminRootComponent,
        ProductEditComponent,
        RootComponent,
        UserRootComponent,
        DailydealsCarouselComponent,
        ProductPageComponent,
        ConfirmAddressComponent,
        PaymentComponent,
        ShippingAddressComponent,
        CartComponent,
        ViewOrdersUserComponent,
        OrdersListComponent,
        ConfirmOrderComponent,
        ThankYouComponent,
        OrderDetailsComponent,
        ShoppingCartComponent,
        UpdatePasswordComponent
    ],
    imports: [
        BrowserModule,
        AngularFireModule.initializeApp(environment.firebase),
        AngularFireAuthModule,
        AngularFirestoreModule,
        AngularFireStorageModule,
        FormsModule,
        Ng2CarouselamosModule,
        ReactiveFormsModule,
        NgbModule.forRoot(),
        RouterModule.forRoot([
            {
                path: '', component: RootComponent,
                children: [
                    {path: '', component: HomeComponent},
                    {path: 'signup', component: SignupComponent},
                    {path: 'login', component: LoginComponent},
                    {path: 'forgot-password', component: ForgotPasswordComponent},
                    {path: 'search/:query', component: SearchResultComponent},
                ]
            },
            {
                path: 'user', component: UserRootComponent, canActivate: [AuthGuard],
                children: [
                    {path: 'profile', component: ProfileDisplayComponent},
                    {path: 'update-address', component: UpdateAddressComponent},
                    {path: 'view-orders', component: ViewOrdersUserComponent},
                    {path: 'confirm-address', component: ConfirmAddressComponent},
                    {path: 'payment', component: PaymentComponent},
                    {path: 'shipping-address', component: ShippingAddressComponent},
                    {path: 'confirm-order', component: ConfirmOrderComponent},
                    {path: 'confirm-order/thank-you/:ordernb', component: ThankYouComponent},
                ]
            },
            {
                path: 'admin', component: AdminRootComponent, canActivate: [AdminGuard],
                children: [
                    {path: 'categories', component: CategoryComponent},
                    {path: 'products/edit/:product_uid', component: ProductEditComponent},
                    {path: 'products/create', component: ProductCreateComponent},
                    {path: 'Order-view/status', component: OrdersListComponent},
                    {path: 'order-details/orderId/:orderId', component: OrderDetailsComponent},
                ]
            },
            {path: 'products', component: ProductsComponent},
            {path: 'product-page/:pid', component: ProductPageComponent},
            {path: 'products/:category', component: ProductsComponent},
            {path: 'check-out', component: CheckOutComponent},
            {path: 'UserCart', component: CartComponent},
            {path: '**', component: PageNotFoundComponent},

        ])
    ],
    providers: [
        HttpClientModule,
        AuthGuard,
        AdminGuard,
        CategoryService,
        UserService,
        AuthService,
        ProductAdminService,
        ProductService,
        CartService,
        OrderUserService,
        OrderService,
    ],
    bootstrap: [AppComponent],

    entryComponents: [
        ConfirmExitModalComponent
    ],

})
export class AppModule {
}