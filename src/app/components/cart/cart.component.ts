import { Component, OnInit } from '@angular/core';
import { CartService } from '../../services/cart.service';
import { ProductService } from 'src/app/services/product.service';
import { Product } from 'src/app/models/product.model';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})

export class CartComponent implements OnInit {
  // Products in cart can be listed
  // Products can be deleted from the cart
  // Quantity is bound now
  // Remove product updates without reload
  // Total price can be seen in the click of a button
  // (UPDATE) Total price can be received dynamically with the change of quantity
  // All errors managed
  // Bootstrap alert added

  /////
  private alerts: string[]
  private alertMessage: string
  public quantityEdited: boolean
  /////

  public productArray: Product[]
  private products: cartItem[]
  // Quantity is supposed to be saved in the array here
  private quantity: number
  private quantityPrice: number[]
  private totalPrice: number = null
  // Get product PID and get quantity..
  // Check if quantity is less than entered quantity on change
  // If not, then Throw error

  constructor(private cartService: CartService,
    private productService: ProductService) {
    this.initializeEverything()
  }

  // A method to calculate the total price of products in the cart...
  getTotalPrice(): number {
    this.totalPrice = 0
    this.quantityPrice = new Array(this.products.length)
    for (let i = 0; i < this.products.length; i++) {
      this.quantityPrice[i] = this.products[i].Quantity * this.productArray[i].price
      this.totalPrice += this.quantityPrice[i]
    }
    console.clear()
    return this.totalPrice
  }

  // What happens is that you can update the quantity and the price is only added and not "changed" (FIXED)
  async checkQuantity(pid: string) {
    this.quantity = this.products[this.cartService.getCartItemIndex(pid)].Quantity
    // To check if the quantity is less than the product quantity (Done)
    // Checks if the quantity is less than the product quantity and then throws alert 
    if (this.quantity <= this.productArray[this.cartService.getCartItemIndex(pid)].quantity && this.quantity > 1) {
      // Total quantity must not always update 
      this.cartService.setQuantity(pid, this.quantity)
      this.quantityEdited = true
      this.alertMessage = this.alerts[0]
      setTimeout(function () {
        this.quantityEdited = false;
      }.bind(this), 4000);

    }
    else if (this.quantity < 1) {
      this.quantityEdited = true
      this.alertMessage = this.alerts[2]
      setTimeout(function () {
        this.quantityEdited = false;
      }.bind(this), 4000);

    }
    else {
      this.quantityEdited = true
      this.alertMessage = this.alerts[1] + this.productArray[this.cartService.getCartItemIndex(pid)].quantity
      setTimeout(function () {
        this.quantityEdited = false;
      }.bind(this), 4000);

    }
  }

  //Method to get the products from the cart Item array
  async getProductFromPID(PID: string) {
    let productPromise = this.productService.getProduct(PID)
    await productPromise.then(product => {
      this.productArray.push(product)
    })
  }

  // A method to get the totalPrice dynamically
  ngOnChanges() {
    this.getTotalPrice()
  }

  //Method to convert the cartItems to products for display
  getCartProducts() {
    let prodDB = this.cartService.getItemsFromCart();
    this.products = prodDB
    for (let i = 0; i < this.products.length; i++) {
      this.products[i].Quantity = this.quantity
    }
    for (let item of prodDB) {
      this.getProductFromPID(item.PID)
    }
  }

  // Just initializes all the variables to avoid TypeError: "undefined" 
  initializeEverything() {
    this.products = new Array()
    this.productArray = new Array()
    this.alerts = new Array("Quantity is updated..", "Enter a quantity less than ", "Enter a quantity greater than 0...")
    this.quantityEdited = false
    this.quantity = 1
  }

  // remove product from cart
  removeProduct(pid: string) {
    this.cartService.removeItemFromCart(pid);
    this.ngOnInit()
  }

  // The reloading issue still exists (FIXED)
  ngOnInit() {
    this.totalPrice = this.getTotalPrice()
    this.initializeEverything()
    this.cartService.init().then(resolve => {
      this.getCartProducts();
    })
  }
}