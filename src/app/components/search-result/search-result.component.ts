import { Component, OnInit } from '@angular/core';
import { Product } from '../../models/product.model';
import { ActivatedRoute } from "@angular/router";
import { ProductService } from 'src/app/services/product.service';

@Component({
    selector: 'app-search-result',
    templateUrl: './search-result.component.html',
    styleUrls: ['./search-result.component.css']
})
export class SearchResultComponent implements OnInit {
    filteredProducts: Product[] = [];
    products: Product[] = [];
    query: string;


    // tslint:disable-next-line:no-shadowed-variable
    constructor(
        private productService: ProductService,
        private route: ActivatedRoute) {

        productService.getProducts();

        this.filteredProducts = (this.query) ?
            this.products.filter(p => p.name.toLowerCase().includes(this.query)) :
            this.products;

        console.clear()



    }

    ngOnInit() {
        console.clear()
        this.productService.getProducts()
        this.filterProducts();
    }

    filterProducts() {
        this.query = this.route.snapshot.paramMap.get("query").toLowerCase();

        this.productService.getProducts().subscribe(products => {
            this.products = products;

            this.route.paramMap.subscribe(params => {
                this.query = params.get("query").toLowerCase();

                this.filteredProducts = (this.query) ?
                    this.products.filter(p => p.name.toLowerCase().includes(this.query)) :
                    this.products;
                return this.filteredProducts;
            });
        });
        /*
        this.productService.getProducts().forEach(
            products => products.forEach(
                product => {
                    if (product.name.toLowerCase().includes(this.query)) this.filteredProducts.push(product);
                }
            ))
        */
    }
}
