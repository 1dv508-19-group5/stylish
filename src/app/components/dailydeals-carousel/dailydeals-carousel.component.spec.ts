import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DailydealsCarouselComponent } from './dailydeals-carousel.component';

describe('DailydealsCarouselComponent', () => {
  let component: DailydealsCarouselComponent;
  let fixture: ComponentFixture<DailydealsCarouselComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DailydealsCarouselComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DailydealsCarouselComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
