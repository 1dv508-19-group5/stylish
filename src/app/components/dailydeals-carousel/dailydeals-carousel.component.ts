import { ProductService } from 'src/app/services/product.service';
import { Component, OnInit, Input } from '@angular/core';
import { Product } from "src/app/models/product.model";
import { Observable } from 'rxjs';

@Component({
  selector: 'app-dailydeals-carousel',
  templateUrl: './dailydeals-carousel.component.html',
  styleUrls: ['./dailydeals-carousel.component.css']
})
export class DailydealsCarouselComponent implements OnInit {

  items: Product[];

  constructor(private productService: ProductService) {
  }

  getProductIDs() {
    this.productService.getProducts().subscribe(
      resultArray => {
        this.items = resultArray.filter(product => product.dealOfTheDay);
      },
      error => console.log("Error::" + error)
    )
  }

  ngOnInit() {
    this.getProductIDs();
  }

  

}
