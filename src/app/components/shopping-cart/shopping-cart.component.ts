import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Product } from 'src/app/models/product.model';
import { CartService } from 'src/app/services/cart.service'
import { ProductService } from 'src/app/services/product.service'
@Component({
  selector: 'app-shopping-cart',
  templateUrl: './shopping-cart.component.html',
  styleUrls: ['./shopping-cart.component.css']
})
export class ShoppingCartComponent implements OnInit {
  // Im supposed to call the service from this component and display the products selected for a specific user in this screen
  // The user is also allowed to edit the quantity of the products he can add to cart.
  // He can also delete a previously selected product from his cart.(Alert needed when he does that)   

  private quantity: number; // Gets number from the text box
  private cartProduct: cartItem; // Cart item has PID and quantity
  private shoppingCart: cart; // Initialize products
  private prodDB: cartItem[];
  public productArray: Product[];
  // His method returns the cartItem array....from which we have to get the product and the product name 


  // What Im trying to do is...
  // Get the cart Item array
  // For each PID from the cart Item array get the products
  // Put all products in a product array
  // Iterate through the product array and display the names

  constructor(private cartService: CartService,
    private productService: ProductService) {
    console.log("Products in cart loaded...")
    this.prodDB = this.cartService.getItemsFromCart();
    console.log(this.prodDB)
    // Gets the converted products 
    this.productArray = this.getProductsFromCart();

  }

  // Get product PID and get quantity..
  // Check if quantity is less than entered quantity on change
  // If not, then Throw error
  checkQuantity() {
    return null;
  }

  //Method to get the products from the cart Item array
  getProductsFromCart(): Product[] {
    for (let item of this.prodDB) {
      let productPromise = this.productService.getProduct(item.PID)
      productPromise.then(product => {
        this.productArray.push(product)
      })
    }
    return this.productArray;
  }

  ngOnInit() {
    console.log("Angular is initiated...")
    this.productArray = new Array()
    this.cartProduct = {
      PID: "",
      Quantity: null
    }
    this.shoppingCart = { Products: null };
  }

}