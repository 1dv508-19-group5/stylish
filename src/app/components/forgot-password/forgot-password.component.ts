import { Component, OnInit } from '@angular/core';
import { ViewChild, ElementRef } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.css']
})
export class ForgotPasswordComponent implements OnInit {
  @ViewChild('reset_password_mail_sent') alert: ElementRef;
  @ViewChild('emailinput') emailInput: ElementRef;
  @ViewChild('reset_password_submit') resetPasswordSubmit: ElementRef;
  @ViewChild('email_not_found') emailNotFound: ElementRef;

  constructor (private service: AuthService) { }

  ngOnInit() {
  }
  
  submitted () {
    this.emailNotFound.nativeElement.hidden = true;

    this.service.resetPassword(this.emailInput.nativeElement.value)
      .then(() => {
        this.alert.nativeElement.hidden = false;
        this.resetPasswordSubmit.nativeElement.disabled = true;
        this.emailInput.nativeElement.disabled = true;
      })
      .catch((err) => {
        if (err.code === "auth/user-not-found") {
          this.emailNotFound.nativeElement.hidden = false;
        }
      })
  }
  
  closeAlert() {
    this.alert.nativeElement.hidden = true;
  }
}
