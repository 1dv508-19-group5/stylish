import {Component} from '@angular/core';
import {OrderService} from 'src/app/services/order.service';

@Component({
    selector: 'app-orders-list',
    templateUrl: './orders-list.component.html',
    styleUrls: ['./orders-list.component.css']
})
export class OrdersListComponent {
    orders = [];
    private size: number;

    constructor(private orderService: OrderService) {
        orderService.getOrders().subscribe(
            (next) => {
                this.orders = next;
            },
            (error) => {
                console.log(error);
            }
        );
    }

    statusChanged(elem, event) {
        elem.statusTmp = event.target.value;
    }

    submit(elem) {
        if (elem.statusTmp === undefined) {
            return;
        }
        this.orderService.updateOrder(elem.id, elem.statusTmp)
            .then((next) => {
                elem.status = elem.statusTmp;
            })
            .catch((error) => {
                console.log(error);
            })
        ;
    }
}
