import { Component, OnInit } from '@angular/core';
import { ProductAdminService } from 'src/app/services/product-admin.service';
import { Category } from 'src/app/models/category.model';
import { Observable } from 'rxjs';
import { CategoryService } from 'src/app/services/category.service';
import { ActivatedRoute } from '@angular/router';
import { Product } from 'src/app/models/product.model';
import { ProductService } from 'src/app/services/product.service';
import { AngularFirestore } from '@angular/fire/firestore';

@Component({
  selector: 'app-product-edit',
  templateUrl: './product-edit.component.html',
  styleUrls: ['./product-edit.component.css']
})
export class ProductEditComponent implements OnInit {
  // basically the form template from the HTML
  productData: Product;
  selectedCategory: Category;
  categoryDB: Observable<Category[]>;
  private prodID: string;
  private quantity: number
  public edited = false;
  private alertMessage: string
  file: any;


  // Just a small problem.. the image gets uploaded to firebase as soon as the user chooses it. (Fixed)
  onFileUpload(event) {
    this.file = event.target.files[0]
  }

  constructor(private pas: ProductAdminService,
    private afs: AngularFirestore,
    private prodService: ProductService,
    private cas: CategoryService,
    private route: ActivatedRoute) {
    console.clear()
    // Used to get the categories to be displayed on the drop down list
    this.categoryDB = this.cas.getCategories();

  }

  onSubmit() {
    console.clear()
    // When the save button is clicked this will update the product contents
    this.edited = true;
    setTimeout(function () {
      this.edited = false;
    }.bind(this), 3000);
    this.quantity = this.productData.quantity
    if(this.quantity < 1){
      this.alertMessage= "Quantity cannot be less than one.."
      return
    }
    // gets selected category
    if (this.selectedCategory.name != null) {
      var cat = this.afs.doc('categories/' + this.selectedCategory.name)
      this.productData.category = cat.ref;
    }
    if (this.file == null) {
      this.pas.updateProduct(this.prodID, this.productData)
      this.alertMessage = "Your changes have been saved...."
    } else {
      this.pas.uploadImage(this.file, this.prodID).then(imageURL => {
        this.productData.imageURL = imageURL.toString();
        this.pas.updateProduct(this.prodID, this.productData)
        this.alertMessage = "Your changes have been saved...."
      }).catch(err => { console.log(err) });
    }
  }

  ngOnInit() {
    this.selectedCategory = {
      id: "",
      name: ""
    }
    this.productData = {
      id: "",
      name: "",
      description: "",
      price: null,
      size: "",
      category: null,
      quantity: null,
      imageURL: "",
      rating: null,
      ratings: null,
      dealOfTheDay: false
    };
    console.clear()
    this.loadData();
  }

  async loadData() {
    this.prodID = this.route.snapshot.paramMap.get("product_uid");
    await this.prodService.getProduct(this.prodID).then(product => {
      this.productData = product;
      this.selectedCategory.name = product.category.id;
    });
  }


  dealOn() {
    let date = new Date();
    let h = date.getHours();

    if (h != 23) {
      this.productData.dealOfTheDay = true;
      this.pas.updateProduct(this.prodID, this.productData);
    }
    else {
      this.productData.dealOfTheDay = false;
      this.pas.updateProduct(this.prodID, this.productData);
    }

  }

}
