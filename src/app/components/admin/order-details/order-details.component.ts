import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {AngularFirestore} from '@angular/fire/firestore';
import {OrderService} from '../../../services/order.service';
import {Order} from '../../../models/order.model';

@Component({
    selector: 'app-order-details',
    templateUrl: './order-details.component.html',
    styleUrls: ['./order-details.component.css']
})
export class OrderDetailsComponent implements OnInit {
    order: any = {};
    private readonly orderID: string;

    constructor(public DB: AngularFirestore, private route: ActivatedRoute, private orderService: OrderService) {
        this.orderID = this.route.snapshot.paramMap.get('orderId');
    }

    ngOnInit() {
        console.log(this.orderID);
        this.orderService.getOrderDetails(this.orderID).subscribe((next) => {
                this.order = next.data();
            }, (error) => {
                console.log(error);
            }
        );
    }

}
