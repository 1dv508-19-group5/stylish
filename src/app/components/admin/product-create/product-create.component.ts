
import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { ProductAdminService } from 'src/app/services/product-admin.service';
import { Product } from 'src/app/models/product.model';
import { Category } from 'src/app/models/category.model'
import { CategoryService } from 'src/app/services/category.service'
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireStorage } from '@angular/fire/storage';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { DocumentReference } from '@angular/fire/firestore';
import { ActivatedRoute } from '@angular/router';
import { formArrayNameProvider } from '@angular/forms/src/directives/reactive_directives/form_group_name';
import { stringify } from '@angular/core/src/render3/util';


@Component({
  selector: 'app-product-create',
  templateUrl: './product-create.component.html',
  styleUrls: ['./product-create.component.css']
})
export class ProductCreateComponent implements OnInit {
  categories: Category;
  categories$;
  profileFrm: FormGroup;
  prod: Observable<Product[]>;
  isSubmitted = false;
  isSuccess = false;
  allFields = true;

  
  public imagePath;
  imgURL: any;
  public message: string;
  image;
  product :Product;

  
  selectedCategory: Category;
  categoryDB: Observable<Category[]>;
  private prodID : string;
  

  constructor(private afs: AngularFirestore,
    private productAdminService: ProductAdminService, 
    private categoryService : CategoryService,
    private route: ActivatedRoute) {
  
      // Used to get the categories to be displayed on the drop down list
      this.categoryDB = this.categoryService.getCategories();
      console.clear()
    }

  
  ngOnInit() {
    this.selectedCategory = {
      id: "",
      name: "accessories"
    }
    this.product = {
      name: "",
      description: "",
      price: null,
      size: "",
      category: null,
      quantity: null,
      dealOfTheDay : false
    };
    console.clear()
  }

  preview(files) {
    if (files.length === 0)
      return;
 
    var mimeType = files[0].type;
    if (mimeType.match(/image\/*/) == null) {
      this.message = "Only images are supported.";
      return;
    }
 
    var reader = new FileReader();
    this.imagePath = files;
    reader.readAsDataURL(files[0]); 
    reader.onload = (_event) => { 
      this.imgURL = reader.result; 
    }
  } 

  onFileUpload(event){
    const file = event.target.files[0];
    this.image = file;
  }

  
  onSubmit() {
    console.clear()
    var category = this.afs.doc("categories/"+this.selectedCategory.name)
    this.product.category = category.ref;
    this.isSubmitted = true;
    
    if (this.product.name != '' && this.product.description != '' && this.product.price > 0 && this.product.size != '' && this.product.quantity > 0){
      
      console.log("Changes Saved");
     
      this.productAdminService.createProductWithImage(this.product, this.image);
      this.isSuccess = true;
      this.allFields = true;
      setTimeout(function () {
        this.isSuccess = false;
        console.log(this.isSuccess );
        window.location.replace("/admin");
      }, 5000);
    }
    else {
      console.log("err");
      this.allFields = false;
    }
    
  }

  isAllFields(){
    return this.allFields;
  }


  


}


