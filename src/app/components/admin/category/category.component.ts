import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {CategoryService} from '../../../services/category.service';
import {Category} from '../../../models/category.model';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.css']
})
export class CategoryComponent implements OnInit {
  profileForm: FormGroup;
  submitted = false;
  success = false; // look if all values have been validate
  prodDB: Observable<Category>;
  id = 0;
  users: Category = {
    name: '',
    id: ''
  };
  editState = false;
  itemToEdit: Category;

  constructor(private formBuilder: FormBuilder, private categoryService: CategoryService) {
    this.profileForm = this.formBuilder.group({
      name: ['', Validators.required]
    });
    console.clear()

  }

  ngOnInit() {
    this.prodDB = this.categoryService.getCategories();
  }

  onSubmit() {
    this.submitted = true;
    if (this.profileForm.invalid) {
      return;
    }
    this.success = true;
    // tslint:disable-next-line:triple-equals
    if (this.users.name != '') {
      this.users.id = String(this.id++);
      this.categoryService.createCategory(this.users);
      this.users.name = '';
    }
  }

  deleteCategory(category: Category){
    this.categoryService.deleteCategory(category);
  }

}
