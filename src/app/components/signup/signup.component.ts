import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from "@angular/router";
import { AuthService } from 'src/app/services/auth.service';
import { confirmPasswordValidator } from "./confirmPasswordValidator";

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {
  register: FormGroup;
  result: any = null;
  submitHasBeenClicked: boolean = false;

  @ViewChild("confirmation_email_sent") confirmationEmailSent: ElementRef;
  
  constructor(
    private frmBuilder: FormBuilder,
    private authService: AuthService,
    private router: Router) {
  }
  
  ngOnInit(){
    if (this.authService.isLoggedIn()) {
      this.router.navigate(['/']);
    }

    this.register = this.frmBuilder.group({
      email: ["", [Validators.required, Validators.email]],
      password: ["", [Validators.required, Validators.minLength(6)]],
      verify: ["", Validators.required]
    }, {
      validator: confirmPasswordValidator("password", "verify")
    });
  }
  
  get email() { return this.register.get('email'); }
  get password() { return this.register.get('password'); }
  get verify() { return this.register.get('verify'); }
  get haveBeenSubmitted () { return this.submitHasBeenClicked }
  
  signUp () {
    if (!this.register.valid) {
      this.submitHasBeenClicked = true;
      return;
    }

    let confirmationEmailSent = this.confirmationEmailSent;

    this.authService.signUp(this.email.value, this.password.value)
      .then(res => this.onSuccessfulSignup(res))
      .catch((err) => {
        console.log(err);
      })
     
  }

  private onSuccessfulSignup (res) {
    this.confirmationEmailSent.nativeElement.hidden = false;
  }

}
