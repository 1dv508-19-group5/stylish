import { Component, OnInit, NgZone} from '@angular/core';
import { Observable } from 'rxjs';
import { ActivatedRoute } from '@angular/router';

import { CategoryService } from 'src/app/services/category.service';

import { Category } from 'src/app/models/category.model';
import { Product } from 'src/app/models/product.model';
import { ProductService } from 'src/app/services/product.service';
import { AuthService } from 'src/app/services/auth.service';
import { UserService } from 'src/app/services/user.service';
import { ProductAdminService } from 'src/app/services/product-admin.service';


@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {
  categories: Observable<Category>;
  activeCategory: string;
  products: Product[];
  filteredProducts: Product[];
  pager: any = {};
  pagedItems: any[];
 
  


  constructor (
    private categoryService: CategoryService,
    private route: ActivatedRoute,
    private productService: ProductService,
    private ngZone: NgZone,
    private authService: AuthService,
    public userService: UserService,
    private productAdminService: ProductAdminService
  ) {
    this.categories = this.categoryService.getCategories();
    this.activeCategory = this.route.snapshot.paramMap.get("category");
    console.clear()
  }

  ngOnInit() {
    this.productService.getProducts().subscribe(
      products => {
        this.products = products;
        this.route.paramMap.subscribe(params => {
        this.activeCategory = params.get("category");
        this.ngZone.run(() =>{
          this.filterProducts();
        })
       
        console.log(this.filteredProducts);
        console.log(products);
        this.setPage(1);
      })
    });
    console.clear()
  }

  setPage(page: number) {
    // get pager object from service
    console.log(this.filteredProducts.length);
    this.pager = this.productService.getPager(this.filteredProducts.length, page);
    console.clear()
    // get current page of items
    this.pagedItems = this.filteredProducts.slice(this.pager.startIndex, this.pager.endIndex + 1);
  }
  filterProducts(){
    var checkbox = document.getElementById('ratings') as HTMLInputElement;
    this.ngZone.run(() => {
      
    })
    if(!checkbox.checked){
      console.log(checkbox.checked)
      this.filteredProducts = (this.activeCategory) ? this.products.filter(p => p.category.path.endsWith(this.activeCategory.toLowerCase())) :
      this.products;
    }else{
      console.log(checkbox.checked);
      this.filteredProducts = (this.activeCategory) ? this.products.filter(p => p.category.path.endsWith(this.activeCategory.toLowerCase()) && p.rating >=4) :
      this.products.filter(p => p.rating >= 4);
    }
    console.clear()
    this.setPage(1);
  }
  deleteProduct(product: Product){
    this.productAdminService.deleteProduct(product.id);
  }
}
