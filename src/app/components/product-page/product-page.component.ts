import { UserService } from 'src/app/services/user.service';
import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ProductService } from '../../services/product.service';
import { CartService } from 'src/app/services/cart.service'
import { Product } from "src/app/models/product.model";
import { AuthService } from 'src/app/services/auth.service'

@Component({
  selector: 'app-product-page',
  templateUrl: './product-page.component.html',
  styleUrls: ['./product-page.component.css']
})
export class ProductPageComponent implements OnInit {
  private pid: string;
  private pnf: boolean;
  private alertMessage: string
  public quantityEdited: boolean
  public product: Product;
  public userRate;
  @ViewChild('rated') alert: ElementRef;
  @ViewChild('error') error: ElementRef;
  @ViewChild('errorBought') errorBought: ElementRef;
  
  constructor(
    private route: ActivatedRoute,
    private authService: AuthService,
    private productService: ProductService,
    private userService: UserService,
    private cartService: CartService
  ) {
    route.paramMap.subscribe(params => {//get PID from the URL params
      this.pid = params.get('pid')
    })
    console.clear()
  }

  // Just a method to get the product without lag
  async getProduct() {
    let productPromise = this.productService.getProduct(this.pid); //get product from PID
    await productPromise.then(product => {
      if (product == null) { this.pnf = true; }
      this.product = product;
    }).catch(reason => {
      this.pnf = true;
      this.pid = null;
    });
    console.clear()
  }

  validateRating(value) {

    this.userRate = value;

    var totalReview = Object.keys(this.product.ratings).length
    var profileRef = this.userService.getProfileData()
    var userID = profileRef.uid

    const index = Object.values(this.product.ratings).findIndex(role => role === userID);

    const indexVoted = Object.values(this.product.ratings).findIndex(role => role === userID);
    
    if (this.product.bought === undefined){
      // bought array not in database (i.e no one has bought this product yet)
      var indexBought=-1
    }
    else {
      var indexBought = Object.values(this.product.bought).findIndex(role => role === userID);
    }
    
    if (indexBought != -1){
      // user bought product (user id in bought array), he is allowed to vote
      if (indexVoted === -1) {
          // the user is not in the ratings array (i.e he didn't vote)
          var newrating = Math.round(((totalReview * this.product.rating) + value) / (totalReview + 1) * 100) / 100
          
          this.alert.nativeElement.hidden = false;
          this.productService.setRating(newrating, this.pid, userID)
      }
      else {
          // user in ratings array (i.e he has already voted) show alert
          this.error.nativeElement.hidden = false;
      } 

    }

  }

  addProductToCart() {
    this.cartService.addItemToCart(this.pid)
   }

  addToCart() {
    this.cartService.init().then(resolve => {
      this.addProductToCart()
      this.quantityEdited = true
      this.alertMessage = "Product is placed in cart.."
      setTimeout(function () {
        this.quantityEdited = false;
      }.bind(this), 4000);})
  
  }
  closeAlert() {
    this.alert.nativeElement.hidden = true;
    this.error.nativeElement.hidden = true;
    this.errorBought.nativeElement.hidden = true;
  }

 
  initializeEverything() {
    this.product = {
      id: "",
      name: "",
      description: "",
      price: null,
      size: "",
      category: null,
      quantity: null,
      imageURL: "",
      rating: null,
      ratings: null,
      dealOfTheDay: false
    }

  }
  ngOnInit() {
    console.clear()
    this.initializeEverything()
    this.getProduct()
  }
}