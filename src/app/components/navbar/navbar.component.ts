import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { UserService } from '../../services/user.service';
import { Observable } from 'rxjs';
import { Router } from "@angular/router";
import * as firebase from 'firebase';
import { query } from '@angular/core/src/render3';



@Component({
  selector: 'navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  user$: Observable<firebase.User>;
  query= "";

  constructor(
    public authService: AuthService,
    private router: Router,
    public userService: UserService
  ) {}


  ngOnInit() {
  }

  signOut() {
    this.authService.signOut().then(res => {
        this.router.navigate(['/']);
    })
  }

  search(){
    this.router.navigate(['/search/' + encodeURIComponent(this.query)]);
    return false;
  }
}
