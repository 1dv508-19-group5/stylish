import { Component, OnInit } from '@angular/core';
import { ViewChild, ElementRef } from '@angular/core';
import { Location } from '@angular/common';
import { Router } from "@angular/router";

import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  email = "";
  password = "";
  
  @ViewChild('user_not_found') userNotFound: ElementRef;
  @ViewChild('wrong_password') wrongPassword: ElementRef;
  @ViewChild('unverified_email') unverifiedEmail: ElementRef;

  constructor (
    private authService: AuthService,
    private location: Location,
    private router: Router
  ) { }

  ngOnInit () {
    if (this.authService.isLoggedIn()) {
      this.router.navigate(['/']);
    }
  }

  logIn () {
    this.userNotFound.nativeElement.hidden = true;
    this.wrongPassword.nativeElement.hidden = true;
    this.unverifiedEmail.nativeElement.hidden = true;

    this.authService.logIn(this.email, this.password)
      .then(() => this.location.back())
      .catch(err => {
        console.log(err)
        if (err.code === "auth/wrong-password") {
          this.wrongPassword.nativeElement.hidden = false;
        }
        else if (err.code === "auth/user-not-found") {
          this.userNotFound.nativeElement.hidden = false;
        }
        else if (err.code === "unverified-email") {
          this.unverifiedEmail.nativeElement.hidden = false;
        }
      });
  }
}
