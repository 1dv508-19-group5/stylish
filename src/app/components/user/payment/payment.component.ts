import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../../services/auth.service';


@Component({
  selector: 'app-payment',
  templateUrl: './payment.component.html',
  styleUrls: ['./payment.component.css']
})
export class PaymentComponent implements OnInit {

  private name: string;
  private number: string;
  private cvv: string;
  private month: string;
  private year : string;
  
  private firstAttempt = true;

  private submitted: boolean = false;

  constructor(private authService: AuthService) { }

  submitPayment() {
    /*
    if (this.isValidForm()) {

      this.submitted = true;

    } else {
      this.firstAttempt = false;
    }
    */
  }

  isValidName(){
    return this.name != undefined && this.name.length > 0;
  }

  isValidNumber(){
    let validNumber: RegExp = /^[+ 0-9]{12}$/;
    return validNumber.test(this.number);
  }

  isValidCvv(){
    let validCvv: RegExp = /^[+ 0-9]{3}$/;
    return validCvv.test(this.cvv);
  }

  isValidMonth(){
    let validMonth: RegExp = /^[+ 0-9]{2}$/;
    return validMonth.test(this.month);
  }

  isValidYear(){
    let validYear: RegExp = /^[+ 0-9]{2}$/;
    return validYear.test(this.year);
  }

  isFirstAttempt() {
    return this.firstAttempt;
  }

  hasBeenSubmitted() {
    return this.submitted;
  }



  isLoggedIn() {
    return this.authService.isLoggedIn();
  }

  ngOnInit() {
  }

  isValidForm() {/*
    return (this.isValidName() && this.isValidNumber() && this.isValidCvv() && this.isValidMonth() && this.isValidYear());
  */
  }

}
