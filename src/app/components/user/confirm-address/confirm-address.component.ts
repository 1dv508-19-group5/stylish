import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../../services/auth.service';
import { UserService } from 'src/app/services/user.service';


@Component({
  selector: 'app-confirm-address',
  templateUrl: './confirm-address.component.html',
  styleUrls: ['./confirm-address.component.css']
})
export class ConfirmAddressComponent implements OnInit {

  constructor(private userService: UserService, private authService: AuthService) { }

  getEmail() {
    var getter = this.userService.getProfileData();
    return getter['email'];
  }

  getName() {
    var getter = this.userService.getProfileData();
    return getter['name'];
  }

  getCity() {
    var getter = this.userService.getProfileData();
    return getter['city'];
  }

  getAddress() {
    var getter = this.userService.getProfileData();
    return getter['address'];
  }

  getZip() {
    var getter = this.userService.getProfileData();
    return getter['zipcode'];
  }

  isLoggedIn() {
    return this.authService.isLoggedIn();
  }


  ngOnInit() {
  }

}
