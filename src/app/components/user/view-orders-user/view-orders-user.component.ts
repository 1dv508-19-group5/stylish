import { OrderUserService } from '../../../services/user-order.service';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Order } from 'src/app/models/order.model';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-view-orders-user',
  templateUrl: './view-orders-user.component.html',
  styleUrls: ['./view-orders-user.component.css']
})
export class ViewOrdersUserComponent implements OnInit {
  orderDB: Observable<Order>;
  

  constructor(private userService: UserService,private OrderService: OrderUserService) {
    
   }

  ngOnInit() {
    this.orderDB = this.OrderService.getOrdersByID();
    
  }

}
