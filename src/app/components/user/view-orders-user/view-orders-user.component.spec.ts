import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewOrdersUserComponent } from './view-orders-user.component';

describe('ViewOrdersUserComponent', () => {
  let component: ViewOrdersUserComponent;
  let fixture: ComponentFixture<ViewOrdersUserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewOrdersUserComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewOrdersUserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
