import { Component, OnInit } from '@angular/core';
import { OrderService } from 'src/app/services/order.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-thank-you',
  templateUrl: './thank-you.component.html',
  styleUrls: ['./thank-you.component.css']
})
export class ThankYouComponent implements OnInit {

  public orderID;

  constructor(
    private orderService: OrderService,
    private route: ActivatedRoute) { 
    this.orderID = this.route.snapshot.paramMap.get("ordernb")
    
  }

  ngOnInit() {
  }

}
