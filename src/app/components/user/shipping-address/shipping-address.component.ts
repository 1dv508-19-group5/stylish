import { Component, OnInit } from '@angular/core';
import { ConfirmExitModalComponent } from 'src/app/components/confirm-exit-modal/confirm-exit-modal.component';
import { UserService } from 'src/app/services/user.service';


@Component({
  selector: 'app-shipping-address',
  templateUrl: './shipping-address.component.html',
  styleUrls: ['./shipping-address.component.css']
})
export class ShippingAddressComponent implements OnInit {
  private name: string;
  private city: string;
  private address: string;
  private zip: string;
  
  private firstAttempt = true;

  private submitted: boolean = false;

  constructor(
    private userService: UserService,
  ) { }

  submitAddress() {

    if (this.isValidForm()) {

      if (this.name != undefined && this.name.length > 0) {
        this.userService.updateName(this.name);
      }

      if (this.isValidCity() && this.isValidAddress() && this.isValidZip()) {
        this.userService.updateEverything(this.address, this.city, this.zip);
      }

      this.submitted = true;

    } else {
      this.firstAttempt = false;
    }
  }

  hasBeenSubmitted() {
    return this.submitted;
  }
  
  // Validators

  isValidCity() {
    return this.city != undefined && this.city.length > 0;
  }

  isValidAddress() {
    return this.address != undefined && this.address.length > 0;
  }

  isValidZip() {
    let validZipNumber: RegExp = /^[+ 0-9]{5}$/;
    return validZipNumber.test(this.zip);
  }

  isFirstAttempt() {
    return this.firstAttempt;
  }

  ngOnInit() {
  }

  isValidForm() {
    return (this.name != undefined && this.name.length > 0) || (this.isValidCity() && this.isValidAddress() && this.isValidZip());
  }
  

 

}
