import { Component, OnInit } from '@angular/core';
import { OrderService } from 'src/app/services/order.service';
import { Order } from 'src/app/models/order.model'
import { CartService } from 'src/app/services/cart.service';
import { Product } from 'src/app/models/product.model';
import { UserService } from 'src/app/services/user.service';
import { AuthService } from 'src/app/services/auth.service';
import { ProductService } from 'src/app/services/product.service';
import { Router } from '@angular/router';
import { ShoppingCartComponent } from '../../shopping-cart/shopping-cart.component';

@Component({
    selector: 'app-confirm-order',
    templateUrl: './confirm-order.component.html',
    styleUrls: ['./confirm-order.component.css']
})
export class ConfirmOrderComponent implements OnInit {
  private cartproducts: cartItem[] = [];
  public productArray: Product[] = [];
  private order:Order;
  private shoppingCart: ShoppingCartComponent;
  constructor(private cartService: CartService , 
              private userService: UserService, 
              private authService: AuthService, 
              private orderService: OrderService,
              private router: Router,
              private productService: ProductService,
            ) {
      this.cartService.init()
      this.cartproducts = cartService.getItemsFromCart();
    
    this.getCartProducts();
  }

  ngOnInit() {
   
  }
  
  //Method to convert the cartItems to products for display
  getCartProducts() {
    let prodDB = this.cartService.getItemsFromCart();
    
    this.cartproducts = prodDB
    
    for (let item of prodDB) {
      this.getProductFromPID(item.PID)
    }
    
    
  }
  //Method to get the products from the cart Item array
  async getProductFromPID(PID: string) {
    let productPromise = this.productService.getProduct(PID)
    await productPromise.then(product => {
      this.productArray.push(product)
    })
  }

  getName() {
    var getter = this.userService.getProfileData();
    return getter['name'];
  }
  getEmail() {
    var getter = this.userService.getProfileData();
    return getter['email'];
  }

  getCity() {
    var getter = this.userService.getProfileData();
    return getter['city'];
  }

  getAddress() {
    var getter = this.userService.getProfileData();
    return getter['address'];
  }

  getZip() {
    var getter = this.userService.getProfileData();
    return getter['zipcode'];
  }
  getUID(){
    var getter = this.userService.getProfileData();
    return getter['uid'];
  }

  isLoggedIn() {
    return this.authService.isLoggedIn();
  }
  getStringDesc(){
    let string = "";
    for(let i=0; i<this.productArray.length;i++){
      if (string != undefined){
        string += " Product: "+this.productArray[i].name+", Quantity: "+this.cartproducts[i].Quantity +" / ";
      }
    }
    return string;
  }
  getFinalPrice(){
    let total:number =0;
    for (let i=0;i<this.productArray.length;i++){
      total+=this.productArray[i].price*this.cartproducts[i].Quantity;
    }
    return total;
  }
  createOrder(){
    this.order = {
      datePlaced: new Date().getDate() +"/"+ (new Date().getMonth()+1)+"/"+ new Date().getFullYear(),
      id: new Date().getTime().toString(), // order id
      uid: this.getUID(), // user id (i.e which user did this order)
      description: this.getStringDesc(), // items bought, quantities
      status: "Ordered", // ordered , shipped , delivered
      address: this.getAddress() + ", "+this.getZip(),
      city: this.getCity(),
      name: this.getName(),
      email: this.getEmail(),
      };
      
      for (let i of this.productArray) {
        this.productService.hasBought(i.id,this.getUID())
        this.cartService.removeItemFromCart(i.id)
      }

      this.orderService.createOrder(this.order);
         
         this.router.navigate(['/user/confirm-order/thank-you/' + encodeURIComponent(this.order.id)]);
               
      
  }

  



}
