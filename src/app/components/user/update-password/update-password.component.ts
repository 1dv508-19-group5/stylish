import { Component, OnInit } from '@angular/core';
import { ConfirmExitModalComponent } from 'src/app/components/confirm-exit-modal/confirm-exit-modal.component';
import { UserService } from 'src/app/services/user.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-update-password',
  templateUrl: './update-password.component.html',
  styleUrls: ['./update-password.component.css']
})
export class UpdatePasswordComponent implements OnInit {

  private oldPass: string;
  private newPass: string;
  private confirmNewPass: string;

  private firstAttempt = true;
  private displayOldPassErrorMessage = false;

  private submitted: boolean = false;

  constructor(
    private userService: UserService,
    private modalService: NgbModal
  ) { }

  ngOnInit() {
    
  }

  submit() {

    if (this.isCorrectOldPassword() && this.isValidNewPass && this.confirmPassMatches()) {
      this.submitted = true;
    } else {
      this.firstAttempt = false;

      if (this.isCorrectOldPassword()) {
        this.displayOldPassErrorMessage = false;
      } else {
        this.displayOldPassErrorMessage = true;
      }

    }
  }

  hasBeenSubmitted() {
    return this.submitted;
  }

  isCorrectOldPassword() {  // To be updated
    return this.oldPass != undefined && this.oldPass.length >= 6;
  }

  isValidNewPass() {
    return this.newPass != undefined && this.newPass.length >= 6;
  }

  confirmPassMatches() {
    return this.newPass === this.confirmNewPass;
  }

  isFirstAttempt() {
    return this.firstAttempt;
  }

  shouldDisplayOldPassError() {
    return this.displayOldPassErrorMessage;
  }

  // Modal Function

  openModal() {
    const modalRef = this.modalService.open(ConfirmExitModalComponent);
  }

}
