import { AuthService } from '../../../services/auth.service';
import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-profile-display',
  templateUrl: './profile-display.component.html',
  styleUrls: ['./profile-display.component.css']
})
export class ProfileDisplayComponent implements OnInit {

  constructor(private userService: UserService, private authService: AuthService) { }

  getEmail() {
    var getter = this.userService.getProfileData();
    return getter['email'];
  }

  getName() {
    var getter = this.userService.getProfileData();
    return getter['name'];
  }

  getCity() {
    var getter = this.userService.getProfileData();
    return getter['city'];
  }

  getAddress() {
    var getter = this.userService.getProfileData();
    return getter['address'];
  }

  getZip() {
    var getter = this.userService.getProfileData();
    return getter['zipcode'];
  }

  isLoggedIn() {
    return this.authService.isLoggedIn();
  }

  ngOnInit() {
  }

}
