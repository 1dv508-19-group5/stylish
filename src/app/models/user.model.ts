export interface User {
  uid: string;
  name: string;
  address: string;
  city: string;
  zipcode: string;
  admin: boolean;
}