import { DocumentReference } from '@angular/fire/firestore';

export interface Product {
  id?: string;
  name: string;
  description: string;
  price: number;
  size: string;
  category: DocumentReference;
  quantity: number;
  imageURL?:string;
  rating?: number;
  ratings?: object; // contains user ids of user that already voted
  bought?: object; // contains user ids of user that bought the product
  dealOfTheDay : boolean;
}