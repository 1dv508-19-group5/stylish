export interface Order {
  datePlaced: string;  
  id: string; // order id
  uid: string; // user id (i.e which user did this order)
  description: string; // items bought, quantities
  status: string; // ordered , shipped , delivered
  address: string;
  city: string;
  name: string;
  email: string
}
