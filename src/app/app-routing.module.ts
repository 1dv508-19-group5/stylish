import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { AuthGuard } from './core/auth.guard';

const routes: Routes = [
  //Example use: { path: 'profile', component: 'profile.component', canActivate: [AuthGuard]},
]

@NgModule({
  declarations: [],
  imports: [
    CommonModule
  ]
})
export class AppRoutingModule {
}
